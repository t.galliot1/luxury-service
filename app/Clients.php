<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clients extends Model
{

    protected $table = 'clients';
    protected $primaryKey = 'id';
    protected $fillable = ['sosietyName', 'typeSociet', 'contactName', 'poste', 'number', 'email', 'notes'];
    protected $dates = ['created_at', 'updated_at'];

    public function clientsJobOffers()
    {
        return $this->hasMany('App\JobOffers');
    }

}
