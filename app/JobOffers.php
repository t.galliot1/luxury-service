<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobOffers extends Model 
{

    protected $table = 'jobOffers';
    protected $primaryKey = 'id';
    protected $fillable = ['societyName', 'contactName', 'contactEmail', 'contactNumber', 'description', 'enable', 'notes', 'jobTitle', 'jobtype', 'location', 'jobCategory', 'closingDate', 'salary', 'client_id'];
    protected $dates = ['created_at', 'updated_at'];

    public function jobOffersCandidacys()
    {
        return $this->hasMany('App\Candidacys');
    }

    public function jobOffersClients()
    {
        return $this->belongsTo('App\Clients');
    }

}