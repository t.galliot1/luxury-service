<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\JobOffers;
use App\Clients;
use App\Candidacys;
use App\Candidates;
use DB;
use Auth;
use Session;

class AdminController extends Controller
{
    protected $jobOffers;
    protected $clients;
    protected $candidacys;
    protected $request;

    public function __construct(JobOffers $jobOffers, Clients $clients, Candidacys $candidacys, Request $request){
        $this->jobOffers = $jobOffers;
        $this->clients = $clients;
        $this->candidacys = $candidacys;
        $this->request = $request;
    }

    public function show(){
        $jobOffers = $this->jobOffers->all();
        $clients = $this->clients->all();
        $candidacys = $this->candidacys->all();
        $isAdmin = DB::select('select admin from candidates where id = ?', [Auth::id()]);

        if (Auth::check()) {
            if (auth()->user()->admin){
                return view('admin', compact('jobOffers', 'clients', 'candidacys'));
            }else{
                Session::flash('status', "vous n'êtes pas admin!");
                return redirect('aboutUs');
            }
        }else{
            Session::flash('status', "vous n'êtes pas connécté!");
            return view('auth.login');
        }
    }

}
