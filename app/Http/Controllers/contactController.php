<?php

namespace App\Http\Controllers;

use Mail;
use Illuminate\Http\Request;

class contactController extends Controller
{
    public function show()
    {
        return view('contact');
    }

    public function send(Request $request)
    {
        $firstName = $request->input('firstname');
        $lastName = $request->input('lastname');
        $email = $request->input('email');
        $phone = $request->input('phone');
        $message = $request->input('message');

        Mail::send('mail', $data = ['firstname' => $firstName, 'lastname' => $lastName, 'email' => $email, 'phone' => $phone, 'message' => $message], function ($m) {
            $m->to('thomas.galliot@hotmail.fr', 'thomasg galliot');
        });


        return back();
    }
}
