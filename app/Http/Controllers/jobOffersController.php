<?php

namespace App\Http\Controllers;

use App\JobOffers;
use Illuminate\Http\Request;

class jobOffersController extends Controller
{
  protected $jobOffers;

  public function __construct(JobOffers $jobOffers){
      $this->jobOffers = $jobOffers;
  }

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {

  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {

  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
    $requestData = $request->all();

    $this->jobOffers->create(array(
      'societyName' => $requestData["Society_Name"],
      'contactName' => $requestData["employer_name"],
      'jobTitle' => $requestData["job_title"],
      'salary' => $requestData["salary"],
      'contactEmail' => $requestData["email_employer"],
      'contactNumber' => $requestData["employer_phone"],
      'description' => $requestData["description"],
      'jobCategory' => $requestData["JobSector"],
      'jobtype' => $requestData["jobType"],
      'location' => $requestData["location"],
    ));

    return back();

  }

  public function show()
  {
    $jobOffers = $this->jobOffers->all();
    return view('jobOffer', compact('jobOffers'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {

  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {

  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {

  }

  public function showDetails($id) {
    $jobOffers = $this->jobOffers->find($id);
    return view('jodDetails', compact('jobOffers'));
  }

}

?>
