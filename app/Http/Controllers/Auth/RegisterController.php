<?php

namespace App\Http\Controllers\Auth;

use App\Candidates;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use DB;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = 'home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'adresse' => 'required|string|max:255',
            'country' => 'required|string|max:255',
            'nationality' => 'required|string|max:255',
            'validPassport' => 'required',
            'passportFile' => 'required|string|max:255',
            'cv' => 'required|string|max:255',
            'Profilpicture' => 'required|string|max:255',
            'currentlocation' => 'required|string|max:255',
            'dateOfBirth' => 'required|string|max:255',
            'placeOfBirth' => 'required|string|max:255',
            'JobSector' => 'required',
            'experience' => 'required|string|max:2555',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'description' => 'required',
            'Availability' => 'required',
            'file' => 'string|max:255',
        ]);
    }

    /**
     * Create a new Candidates instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Candidates
     */
    protected function create(array $data)
    {
        return Candidates::create([
            'firstName' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'genre' => $data['genre'],
            'lastName' => $data['lastname'],
            'Adress' => $data['adresse'],
            'country' => $data['country'],
            'nationality' => $data['nationality'],
            'validPassport' => $data['validPassport'],
            'passportUpload' => $data['passportFile'],
            'cv' => $data['cv'],
            'picProfile' => $data['Profilpicture'],
            'location' => $data['currentlocation'],
            'dateOfBirth' => $data['dateOfBirth'],
            'placeOfBirth' => $data['placeOfBirth'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'Availability' => $data['Availability'],
            'JobSector' => $data['JobSector'],
            'experience' => $data['experience'],
            'notes' => $data['description'],
        ]);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        return $this->registered($request, $user)
                        ?: redirect($this->redirectPath());
    }
}
