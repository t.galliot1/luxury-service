<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Clients;
use App\JobOffers;
use App\Candidacys;
use Session;

class CandidacysController extends Controller
{

  protected $candidacys;

  public function __construct(Candidacys $candidacys)
  {
    $this->candidacys = $candidacys;
  }
  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {

  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {

  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
    if(Auth::check()){
        $requestData = $request->all();

        $this->candidacys->create(array(
          'candidates_id' => Auth::id(),
          'jobOffer_id' => $requestData["jobId"],

        ));
        return back();
    }
    else{
        Session::flash('status', "vous n'êtes pas connécté!");
        return view('auth.login');
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {

  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {

  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {

  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {

  }

}

?>
