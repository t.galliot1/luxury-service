<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Candidacys extends Model 
{

    protected $table = 'candidacys';
    protected $fillable  = ['candidates_id', 'jobOffer_id'];
    protected $dates = ['created_at', 'updated_at'];

    public function candidacysCandidates()
    {
        return $this->belongsTo('App\Candidates');
    }

    public function candidacysJobOffers()
    {
        return $this->belongsTo('App\JobOffers');
    }

}