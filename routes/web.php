<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
})->name('index');



Route::get('/aboutUs', function () {
    return view('aboutUs');
})->name('aboutUs');

Route::get('/homepage', 'Controller@homepage')->name('homepage');
Route::get('/contact', 'contactController@show')->name('contact');
Route::get('/jobDetails/{id}', 'jobOffersController@showDetails');
Route::get('/profil', 'profilController@show')->name('profil');
Route::get('/admin', 'AdminController@show')->name('admin');
Route::get('/logout', 'LogoutController@logout')->name('logout');
Route::post('/storeJob', 'jobOffersController@store');
Route::post('/storeClients', 'ClientsController@store');
Route::get('/joboffer', 'jobOffersController@show')->name('jobOffer');
Route::post('/jobOfferApply', 'CandidacysController@store')->name('jobOfferApply');





Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('candidates', 'CandidatesController');
Route::resource('clients', 'ClientsController');
Route::resource('joboffers', 'jobOffersController');
Route::resource('candidacys', 'CandidacysController');

Route::post('/send', 'contactController@send')->name('send');

Route::get('protected', ['middleware' => ['auth', 'admin'], function() {
    return "this page requires that you be logged in and an Admin";
}]);
