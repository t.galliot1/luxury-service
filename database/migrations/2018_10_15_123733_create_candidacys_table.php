<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCandidacysTable extends Migration {

	public function up()
	{
		Schema::create('candidacys', function(Blueprint $table) {
			$table->timestamps();
			$table->integer('candidates_id')->unsigned();
			$table->integer('jobOffer_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('candidacys');
	}
}