<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateJobOffersTable extends Migration {

	public function up()
	{
		Schema::create('jobOffers', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('societyName', 255);
			$table->string('contactName', 255);
			$table->string('contactEmail', 255);
			$table->string('contactNumber');
			$table->text('description');
			$table->boolean('enable')->default(0);
			$table->text('notes');
			$table->string('jobTitle', 255);
			$table->enum('jobtype', array('Fulltime', 'Parttime', 'Temporary', 'Freelance', 'Seasonal'));
			$table->text('location');
			$table->enum('jobCategory', array('Commercial', 'Retailsales', 'Creative', 'Technology', 'Marketing&PR', 'Fashion&luxury', 'Management&HR'));
			$table->date('closingDate');
			$table->string('salary', 255);
			$table->integer('client_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('jobOffers');
	}
}