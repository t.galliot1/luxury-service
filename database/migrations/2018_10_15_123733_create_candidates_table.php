<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCandidatesTable extends Migration {

	public function up()
	{
		Schema::create('candidates', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->enum('genre', array('Male', 'female'));
			$table->string('firstName', 255);
			$table->string('lastName', 255);
			$table->string('Adress', 255);
			$table->string('country', 255);
			$table->string('nationality', 255);
			$table->boolean('passport')->default(0);
			$table->string('passportUpload', 255);
			$table->string('cv', 255);
			$table->string('picProfile');
			$table->string('location', 255);
			$table->string('dateOfBirth', 255);
			$table->string('placeOfBirth', 255);
			$table->string('email', 255);
			$table->string('confirmEmail', 255);
			$table->string('password', 255);
			$table->string('confirmPassword', 255);
			$table->boolean('Availability')->default(1);
			$table->enum('jobSector', array('Commercial', 'Retailsales', 'Creative', 'Technology', 'Marketing&PR', 'Fashion&luxury', 'Management&HR'));
			$table->enum('experience', array('0-6month', '6month-1year', '1-2years', '2+years', '5+years', '10+years'));
			$table->text('notes');
			$table->date('delete_at');
			$table->string('file', 255);
		});
	}

	public function down()
	{
		Schema::drop('candidates');
	}
}