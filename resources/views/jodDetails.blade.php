@extends('layouts.base')
@section('title', 'Job Offers')
@section('content')
<link href="../assets/css/main.css" rel="stylesheet">

<section class="page-title page-title-bg fixed-bg overlay dark-5 padding-top-160 padding-bottom-80">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="white-text">{{$jobOffers["jobTitle"]}}</h2>
                    <span class="white-text">Ref. {{$jobOffers["id"]}}</span>
                    <ol class="breadcrumb">
                        <li>
                            <div class="portfolio-nav">
                                <a href="#!" class="waves-effect waves-dark"><i class="fa fa-angle-left" aria-hidden="true"></i> Prev</a>
                                <a href="#!" class="waves-effect waves-dark"><i class="fa fa-th-large" aria-hidden="true"></i></a>
                                <a href="#!" class="waves-effect waves-dark">Next <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                            </div>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <!-- Page Content-->
    <section class="single-project-section section-padding light-gray-bg">
        <div class="container">
            <div class="project-overview">
                <div class="row mb-80">
                    <div class="col-xs-12 col-md-8">
                        <p class="ref grey-text no-margin">Ref. {{$jobOffers["id"]}}</p>
                        <h2>{{$jobOffers["jobTitle"]}}</h2>
                        <p>{{$jobOffers["description"]}}</p>
                    </div>

                    <div class="col-xs-12 col-md-4 quick-overview">
                        <ul class="portfolio-meta">
                            <li><span> Pulished at </span>{{$jobOffers["created_at"]}}</li>
                            <li><span> Position </span>{{$jobOffers["jobTitle"]}}</li>
                            <li><span> Contract Type </span>{{$jobOffers["jobtype"]}}</li>
                            <li><span> Salary </span>{{$jobOffers["salary"]}}k &euro;</li>
                            <li><span> Location </span>{{$jobOffers["location"]}}</li>
                            <li><span> Starting date </span>{{$jobOffers["created_at"]}}</li>
                        </ul>
                    <form method="POST" action="{{ route('jobOfferApply')}}">
                        <input type="hidden" name="jobId" value=" {{$jobOffers["id"]}} ">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button class="btn btn-block gradient primary mt-30 waves-effect waves-light" type="submit">Apply for this job</button>
                    </form>
                        <div class="btn btn-block btn-success mt-30 waves-effect waves-light disabled">You have applied for this job</div>
                    </div>
                </div>
            </div>

            <nav class="single-post-navigation no-margin" role="navigation">
                <div class="row">

                    <div class="col-xs-6 col-sm-6 col-md-4">
                        <div class="previous-post-link">
                        <a class="btn border primary waves-effect waves-dark" href="{{$jobOffers["id"] - 1}}">
                                <i class="fa fa-long-arrow-left"></i>Previous
                            </a>
                        </div>
                    </div>
                    <div class="hidden-xs hidden-sm col-md-4"></div>


                    <div class="col-xs-6 col-sm-6 col-md-4">
                        <div class="next-post-link">
                            <a class="btn border primary waves-effect waves-dark" href="{{$jobOffers["id"] + 1}}">
                                Next <i class="fa fa-long-arrow-right"></i>
                            </a>
                        </div>
                    </div>

                </div>
            </nav>
        </div>
    </section>
@endsection
