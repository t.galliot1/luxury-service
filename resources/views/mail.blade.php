<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <ul>
      <li><strong>First name</strong> : {{ $_POST['firstname'] }}</li>
      <li><strong>Last name</strong> : {{ $_POST['lastname'] }}</li>
      <li><strong>Email</strong> : {{ $_POST['email'] }}</li>
      <li><strong>Phone</strong> : {{ $_POST['phone'] }}</li>
      <li><strong>Message</strong> : {{ $_POST['message'] }}</li>
    </ul>
  </body>
</html>
