@extends('layouts.base')

@section('content')
<br><br><br><br>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-warning" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                You are logged in!
            </div>
        <div class="col-md-3">
            <div class="card-header"><a href="profil">You'r profil</a></div>
        </div>
    </div>
</div>
<br><br><br><br>
@endsection
