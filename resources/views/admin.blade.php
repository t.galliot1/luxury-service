@extends('layouts.base')
@section('title', 'Profil')
@section('content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
    <section class="gray-bg">
        <div class="container">
        </div>
    </section>
    <section class="gray-bg">
            <div class="container">
                <div class="row">
                    <div class="score-container">
                        <br><br>
                        <h3>
                            Admin Space
                        </h3>
                    </div>
                </div>
            </div>
            <br><hr>
        </section>
        <section class="gray-bg">
            <div class="container-fluid">
                <div class="row">
                    <div class="col s2"  style="border-right: 1px solid #5b5b5b; box-shadow: 2px 2px 6px black">
                        <ul>
                            <li><a href="#!">option 1</a></li>
                            <li><a href="#!">option 2</a></li>
                            <li><a href="#!">option 3</a></li>
                            <li><a href="#!">option 4</a></li>
                            <li><a href="#!">option 5</a></li>
                            <li><a href="#!">option 6</a></li>
                            <li><a href="#!">option 7</a></li>
                            <li><a href="#!">option 8</a></li>
                        </ul>
                    </div>
                    <div class="col s10" style="height 20px">
                        <canvas id="myChart" height="10" style="border: 1px solid black; width: 100%"></canvas>
                    </div>
                </div>
            </div>
            <br><br>
            </section>
            <section>
                <div class="row">
                    <div class="col s12">
                        <ul class="tabs">
                            <li class="tab col s6"><a class="active"  href="#createJobs">Création d'une offre d'emploi</a></li>
                            <li class="tab col s6"><a href="#cleintsCreator">Clents Creator</a></li>
                        </ul>
                    </div>
                    <div id="createJobs" class="col s12">
                            @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form method="POST" action="storeJob" accept-charset="UTF-8" id="candidateForm" role="form" data-parsley-validate="" enctype="multipart/form-data"><input name="_token" type="hidden" value="UYfchy67WuTVzhSstPK1RDZEIfgqpKCcLnLyhf8a">
                            <section class="section-padding">
                                <div class="container">
                                    <div class="row">
                                        <h3 class="text-extrabold">Création d'une offre d'emploi</h3>
                                        <div class="clearfix visible-sm"></div>
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="col-xs-12 col-sm-6 col-md-4">
                                            <div class="input-field">
                                                <input type="text" class="form-control" name="Society_Name" id="Society_Name" value="" required>
                                                <label for="Society_name">Society Name</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-4">
                                            <div class="input-field">
                                                <input type="text" class="form-control" name="employer_name" id="employer_name" value="" required>
                                                <label for="Employer_name">Employer name</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-4">
                                            <div class="input-field">
                                                <input type="text" class="form-control" name="job_title" id="job_title" value="" required>
                                                <label for="job_title">Job Title</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-4">
                                            <div class="input-field">
                                                <input type="text" class="form-control" name="salary" id="salary" value="" required>
                                                <label for="salary">Salary</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-4">
                                            <div class="input-field">
                                                <input required="required" id="email_employer" name="email_employer" type="text" value="">
                                                <label for="email_employer">Email Employer</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-4">
                                            <div class="input-field">
                                                <label for="employer_phone">Employer Phone</label>
                                                <br>
                                                <input required="required" id="employer_phone" name="employer_phone" type="phone" value="">
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12">
                                            <div class="input-field">
                                                <textarea class="materialize-textarea" id="description" name="description" cols="50" rows="10"></textarea>
                                                <label for="description">Short description for your offers </label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                            <div class="input-field">
                                                <label for="jobSector">job Sector</label>
                                                <br>
                                                <select id="JobSector" type="select" class="form-control{{ $errors->has('JobSector') ? ' is-invalid' : '' }}" name="JobSector" value="{{ old('JobSector') }}" required autofocus>
                                                    <option selected value="Commercial">Commercial</option>
                                                    <option value="RetailSales">Retail sales</option>
                                                    <option value="Creative">Creative</option>
                                                    <option value="Technology">Technology</option>
                                                    <option value="Marketing & PR">Marketing & PR</option>
                                                    <option value="Fashion & Luxury">Fashion & luxury</option>
                                                    <option value="Management & HR">Management & HR</option>
                                                </select>
                                                <span class="help-block">jobSector</span>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                            <div class="input-field">
                                                <label for="jobType">job Type</label>
                                                <br>
                                                <select id="jobType" type="select" class="form-control{{ $errors->has('jobType') ? ' is-invalid' : '' }}" name="jobType" value="{{ old('jobType') }}" required autofocus>
                                                    <option selected value="Fulltime">Fulltime</option>
                                                    <option value="Parttime">Parttime</option>
                                                    <option value="Temporary">Temporary</option>
                                                    <option value="Freelance">Freelance</option>
                                                    <option value="Seasonal">Seasonal</option>
                                                </select>
                                                <span class="help-block">jobType</span>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <div class="input-field">
                                                <input id="location" name="location" type="text" value="">
                                                <label for="location">location</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-4">
                                <button type="submit" class="btn btn-block btn-lg gradient secondary waves-effect waves-light">
                                    <span><strong>CREATE</strong></span>
                                </button>
                            </div>
                        </form>
                    </div>
                    <div id="cleintsCreator" class="col s12">

                            <form method="POST" action="storeClients" accept-charset="UTF-8" id="candidateForm" role="form" data-parsley-validate="" enctype="multipart/form-data"><input name="_token" type="hidden" value="UYfchy67WuTVzhSstPK1RDZEIfgqpKCcLnLyhf8a">
                                <section class="section-padding">
                                    <div class="container">
                                        <div class="row">
                                            <h3 class="text-extrabold">Création d'un client</h3>
                                            <div class="clearfix visible-sm"></div>
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <div class="col-xs-12 col-sm-6 col-md-4">
                                                <div class="input-field">
                                                    <input type="text" class="form-control" name="Society_Name" id="Society_Name" value="" required>
                                                    <label for="Society_name">Society Name</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4">
                                                <div class="input-field">
                                                    <input type="text" class="form-control" name="employer_name" id="employer_name" value="" required>
                                                    <label for="Employer_name">Employer name</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4">
                                                <div class="input-field">
                                                    <input type="text" class="form-control" name="typeSociety" id="typeSociety" value="" required>
                                                    <label for="typeSociety">type of Society</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4">
                                                <div class="input-field">
                                                    <input type="text" class="form-control" name="poste" id="poste" value="" required>
                                                    <label for="poste">Poste</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4">
                                                <div class="input-field">
                                                    <input required="required" id="email_employer" name="email_employer" type="text" value="">
                                                    <label for="email_employer">Email Employer</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4">
                                                <div class="input-field">
                                                    <label for="employer_phone">Employer Phone</label>
                                                    <br>
                                                    <input required="required" id="employer_phone" name="employer_phone" type="phone" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-4">
                                    <button type="submit" class="btn btn-block btn-lg gradient secondary waves-effect waves-light">
                                        <span><strong>CREATE Clients</strong></span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
                <section class="gradient" style="height: 160px; background:#5b5b5b"></section>
                <section>
                    <div class="row">
                        <div class="col s12">
                            <ul class="tabs">
                                <li class="tab col s6"><a class="active"  href="#listJobsOffer">list of Jobs Offers</a></li>
                                <li class="tab col s6"><a href="#listOfClients">List of Clients</a></li>
                                <li class="tab col s6"><a href="#listOfCandidacys">List of Candidacys</a></li>
                            </ul>
                        </div>
                        <div id="listJobsOffer" class="col s12">
                            <table class="responsive-table striped highlight">
                                <thead>
                                    <tr>
                                        <th>Ref.</th>
                                        <th>Created At</th>
                                        <th>Society Name</th>
                                        <th>Job Title</th>
                                        <th>Job Type</th>
                                        <th>Job Category</th>
                                        <th>Salary</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($jobOffers as $jobOffer)
                                    <tr>
                                        <td>{{ $jobOffer["id"] }}</td>
                                        <td>{{ $jobOffer["created_at"] }}</td>
                                        <td>{{ $jobOffer["societyName"] }}</td>
                                        <td>{{ $jobOffer["jobTitle"] }}</td>
                                        <td>{{ $jobOffer["jobtype"] }}</td>
                                        <td>{{ $jobOffer["jobCategory"] }}</td>
                                        <td>{{ $jobOffer["salary"] }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div id="listOfClients" class="col s12">
                            <table class="responsive-table striped highlight">
                                <thead>
                                    <tr>
                                        <th>Created At</th>
                                        <th>Society Name</th>
                                        <th>type Society</th>
                                        <th>contact</th>
                                        <th>post</th>
                                        <th>Email</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($clients as $client)
                                    <tr>
                                        <td>{{ $client["id"] }}</td>
                                        <td>{{ $client["created_at"] }}</td>
                                        <td>{{ $client["societyName"] }}</td>
                                        <td>{{ $client["typeSociety"] }}</td>
                                        <td>{{ $client["contactName"] }}</td>
                                        <td>{{ $client["poste"] }}</td>
                                        <td>{{ $client["email"] }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div id="listOfCandidacys" class="col s12">
                            <table class="responsive-table striped highlight">
                                <thead>
                                    <tr>
                                        <th>Created At</th>
                                        <th>candidate id</th>
                                        <th>jobOffer id</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($candidacys as $candidacy)
                                    <tr>
                                        <td>{{ $candidacy["created_at"] }}</td>
                                        <td>{{ $candidacy["candidates_id"] }}</td>
                                        <td>{{ $candidacy["jobOffer_id"] }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            <script>
                var ctx = document.getElementById('myChart').getContext('2d');
                var chart = new Chart(ctx, {
                    // The type of chart we want to create
                    type: 'line',

                    // The data for our dataset
                    data: {
                        labels: ["January", "February", "March", "April", "May", "June", "July"],
                        datasets: [{
                            label: "My First dataset",
                            backgroundColor: 'rgb(255, 99, 132)',
                            borderColor: 'rgb(255, 99, 132)',
                            data: [0, 10, 5, 2, 20, 30, 45],
                        }]
                    },

                    // Configuration options go here
                    options: {}
                });

            </script>
@endsection
